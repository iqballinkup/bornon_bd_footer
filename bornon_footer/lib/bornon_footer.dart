import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BornonFooter extends StatefulWidget {
  const BornonFooter({super.key});

  @override
  State<BornonFooter> createState() => _BornonFooterState();
}

class _BornonFooterState extends State<BornonFooter> {
  @override
  Widget build(BuildContext context) {
    double headingTextSize = 16.0;
    double descriptionTextSize = 12.0;
    double imageWidth = 32.0;
    double socialCircleAvatarRadius = 20.0;

    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Text(
                  'We Accept',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: headingTextSize,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset(
                    "images/bkash.jpg",
                    width: imageWidth,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Image.asset(
                    "images/rocket.png",
                    width: imageWidth,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Image.asset(
                    "images/dbbl.jpg",
                    width: imageWidth,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Image.asset(
                    "images/visa.png",
                    width: imageWidth,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Image.asset(
                    "images/mastercard.jpg",
                    width: imageWidth,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Image.asset(
                    "images/paypal.png",
                    width: imageWidth,
                  ),
                ],
              ),

              //------------------ Contact us section ---------

              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Contact us',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: headingTextSize,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(Icons.pin_drop, size: 26, color: Colors.white),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Text(
                            'Office: Flat # B4, House # 74, Road # 21, Banani, Dhaka Production House: Flat # 2C & 2D, House # 20, Shahid Minar Road, Kallayanpur, Dhaka.',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: descriptionTextSize,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(Icons.phone, size: 26, color: Colors.white),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Text(
                            '+8801766622202',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: descriptionTextSize,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(Icons.phone, size: 26, color: Colors.white),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Text(
                            '+8801766622202',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: descriptionTextSize,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(Icons.email, size: 26, color: Colors.white),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Text(
                            'bornonlifestyleltd@gmail.com',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: descriptionTextSize,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              //-------------User Information Section---------
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'User Information',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: headingTextSize,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'About Us',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Contact Us',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'My Account',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Cart List',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Checkout',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Login',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //-------------Quick Link Section---------
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Quick Link',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: headingTextSize,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Live Traking Order',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Mission and Vission',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Trams and Condition',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Store Location',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Size Guide',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //-------------Product Category Section---------
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Product Category',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: headingTextSize,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Care',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'New Arrival',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Kids',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Icon(Icons.arrow_forward_ios,
                              size: 20, color: Colors.white),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Trending',
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  fontSize: descriptionTextSize,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Text(
                        'Download Mobile App',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: descriptionTextSize,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
                color: Colors.white,
              ),

              //-----------Footer------

              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Developed By: ',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: descriptionTextSize,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Text(
                          'Link-Up Technology Ltd.',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: descriptionTextSize,
                              decoration: TextDecoration.underline,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          radius: socialCircleAvatarRadius,
                          backgroundColor: Color.fromARGB(255, 84, 84, 84),
                          child: Image.asset(
                            "images/facebook.png",
                            fit: BoxFit.contain,
                            width: 22.0,
                            height: 22.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CircleAvatar(
                          radius: socialCircleAvatarRadius,
                          backgroundColor: Color.fromARGB(255, 84, 84, 84),
                          child: Image.asset(
                            "images/instagram.png",
                            fit: BoxFit.contain,
                            width: 22.0,
                            height: 22.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CircleAvatar(
                          radius: socialCircleAvatarRadius,
                          backgroundColor: Color.fromARGB(255, 84, 84, 84),
                          child: Image.asset(
                            "images/twitter.png",
                            fit: BoxFit.contain,
                            width: 22.0,
                            height: 22.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CircleAvatar(
                          radius: socialCircleAvatarRadius,
                          backgroundColor: Color.fromARGB(255, 84, 84, 84),
                          child: Image.asset(
                            "images/youtube.png",
                            fit: BoxFit.contain,
                            width: 22.0,
                            height: 22.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
